apt-get -y install unzip
wget http://downloads.sourceforge.net/project/sugarcrm/1%20-%20SugarCRM%206.5.X/SugarCommunityEdition-6.5.X/SugarCE-6.5.16.zip 
unzip SugarCE-6.5.16.zip -d /var/www/
mv /var/www/SugarCE-Full-6.5.16 /var/www/sugarcrm

 
chown www-data:www-data /var/www/sugarcrm -R 
chmod 755 /var/www/sugarcrm -R


/bin/echo "Installing MySQL"

$MYSQLADMINPWD=$(date +%s | sha256sum | base64 | head -c 32 ; echo)
sudo debconf-set-selections <<< 'mysql-server-6.5.16 mysql-server/root_password $MYSQLADMINPWD'
sudo debconf-set-selections <<< 'mysql-server-6.5.15 mysql-server/root_password_again $MYSQLADMINPWD'
/usr/bin/apt-get -y install mysql-server php5-mysql mysql-client
		

# To Setup Crontab Note: In order to run Sugar Schedulers, add the following line to the crontab file: 

/bin/cat >> /etc/crontab  <<DELIM
* * * * *     cd /var/www/sugarcrm; php -f cron.php > /dev/null 2>&1 
DELIM

		
# GD Library #IMAP Libraries   #Curl	

sudo apt-get -y install php5-gd php5-imap php5-curl
/etc/init.d/apache2 restart
		
		
	/bin/echo
	/bin/echo
	/bin/echo "Installation Completed.  Now configure SugarCRM via the browser interface"
	/bin/echo
	/bin/echo '  http://'`/sbin/ifconfig eth0 | /bin/grep 'inet addr:' | /usr/bin/cut -d: -f2 | /usr/bin/awk '{ print $1}'`
	/bin/echo "  DB host address: "
		/bin/echo "127.0.0.1"
	/bin/echo "  DB username used: "
		/bin/echo "root"
	/bin/echo "  DB Password used: "
		/bin/echo $PGSQLPASSWORD